#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = ['Click>=6.0', 'pandas>=0.23.4', 'homura>=0.1.5']

setup_requirements = []

test_requirements = ['homura>=0.1.5']

setup(
    author="Victor Ferreira",
    author_email='victor.ferreira@hexgis.com.br',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
    ],
    description="Descricao",
    entry_points={
        'console_scripts': [
            'csv_reader=csv_reader.cli:main',
        ],
    },
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='csv_reader',
    name='csv_reader',
    packages=find_packages(include=['csv_reader']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/victormefer/csv_reader',
    version='0.1.2',
    zip_safe=False,
)
