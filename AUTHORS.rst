=======
Credits
=======

Development Lead
----------------

* Victor Ferreira <victor.ferreira@hexgis.com.br>

Contributors
------------

* Dagnaldo Silva <dagnaldo.silva@hexgis.com.br>
* Wesley Rocha <wesley.rocha@hexgis.com.br>
