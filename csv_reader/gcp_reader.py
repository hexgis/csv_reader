# -*- coding: utf-8 -*-
import os
import subprocess
import pandas as pd
import tempfile

from datetime import datetime, timedelta


class GCPReader:

    @classmethod
    def __get_formatted_dates(cls, start_date, end_date):
        if not start_date:
            start_date = datetime.now() - timedelta(days=15)
            start_date = start_date.date().isoformat()

        if not end_date:
            end_date = datetime.now().date()
            end_date = end_date.isoformat()

        end_date = datetime.strptime(end_date, '%Y-%m-%d')
        end_date = end_date + timedelta(days=1)
        end_date = end_date.date().isoformat()

        return start_date, end_date

    @classmethod
    def __check_gzip_headers(cls, input_file, headers):
        df = pd.read_csv(input_file, nrows=1)
        for h in headers:
            if h not in df.keys():
                raise ValueError("Needed headers missing in input")
        return True

    @classmethod
    def __check_file_exists(cls, input_file):
        if os.path.exists(input_file):
            return True
        else:
            raise ValueError("Input file does not exists")

    @classmethod
    def __check_gzip_integrity(cls, input_file):
        try:
            subprocess.check_call(
                'gzip -t {}'.format(input_file), shell=True)
            return True
        except Exception:
            raise ValueError("Input integrity fail")

    @classmethod
    def __download_csv_file(cls, csv_link, use_gsutil=False):
        if not use_gsutil:
            import homura
            service = "https://storage.googleapis.com/"
            output_path = tempfile.NamedTemporaryFile(suffix=".csv.gz",
                                                      delete=False)
            output_path = output_path.name
            try:
                homura.download(url=os.path.join(service, csv_link),
                                path=output_path)
                return output_path
            except Exception as exc:
                raise exc
        else:
            import subprocess
            service = "gs://"
            output_path = tempfile.TemporaryDirectory()
            try:
                command = "gsutil cp {url} {output_path}".format(
                    url=os.path.join(service, csv_link),
                    output_path=output_path)
                subprocess.check_call(command, shell=True)
                return output_path
            except Exception as exc:
                raise exc

    @staticmethod
    def sentinel2(input_file=None, start_date=None, end_date=None,
                  date_column='SENSING_TIME',
                  tile_column='MGRS_TILE',
                  area_tiles=[],
                  chunksize=10000,
                  headers=['GRANULE_ID', 'PRODUCT_ID', 'MGRS_TILE', 'BASE_URL',
                           'SENSING_TIME', 'CLOUD_COVER', 'GENERATION_TIME']):

        start_date, end_date = GCPReader.__get_formatted_dates(
            start_date, end_date)

        if not input_file:
            try:
                input_file = GCPReader.__download_csv_file(
                    'gcp-public-data-sentinel-2/index.csv.gz')
            except Exception as exc:
                raise(exc)
        try:
            GCPReader.__check_file_exists(input_file)

            GCPReader.__check_gzip_integrity(input_file)

            GCPReader.__check_gzip_headers(input_file, headers)

            for df in pd.read_csv(input_file, chunksize=chunksize,
                                  parse_dates=["GENERATION_TIME",
                                               "SENSING_TIME"]):
                filter_date = df.loc[
                    (df[date_column] >= start_date) &
                    (df[date_column] <= end_date)
                ]

                if area_tiles:
                    filter_date = filter_date.loc[
                        filter_date[tile_column].isin(area_tiles)]

                if not filter_date.empty:
                    yield filter_date.to_dict(orient='index')
        except Exception as exc:
            raise exc

    @staticmethod
    def landsat(input_file=None, start_date=None, end_date=None,
                date_column='DATE_ACQUIRED',
                tile_columns=['WRS_PATH', 'WRS_ROW'],
                area_tiles=[],
                spacecraft='LANDSAT_8',
                chunksize=10000,
                headers=['SCENE_ID', 'PRODUCT_ID', 'SPACECRAFT_ID',
                         'DATE_ACQUIRED', 'COLLECTION_NUMBER',
                         'WRS_PATH', 'WRS_ROW', 'CLOUD_COVER', 'BASE_URL']):

        start_date, end_date = GCPReader.__get_formatted_dates(
            start_date, end_date)

        if not input_file:
            try:
                input_file = GCPReader.__download_csv_file(
                    'gcp-public-data-landsat/index.csv.gz')
            except Exception as exc:
                raise(exc)

        try:
            GCPReader.__check_file_exists(input_file)

            GCPReader.__check_gzip_integrity(input_file)

            GCPReader.__check_gzip_headers(input_file, headers)

            for df in pd.read_csv(input_file, chunksize=chunksize,
                                  parse_dates=["DATE_ACQUIRED"]):
                filter_date = df.loc[
                    (df[date_column] >= start_date) &
                    (df[date_column] <= end_date) &
                    (df['SPACECRAFT_ID'] == spacecraft)
                ]

                paths = [p for p, r in area_tiles]
                rows = [r for p, r in area_tiles]

                if area_tiles:
                    filter_date = filter_date.loc[
                        filter_date[tile_columns[0]].isin(paths) &
                        filter_date[tile_columns[1]].isin(rows)]

                if not filter_date.empty:
                    yield filter_date.to_dict(orient='index')
        except Exception as exc:
                raise exc
