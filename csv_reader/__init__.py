# -*- coding: utf-8 -*-

"""Top-level package for CSV Reader."""

__author__ = """Victor Ferreira"""
__email__ = 'victor.ferreira@hexgis.com.br'
__version__ = '0.1.2'
