==========
CSV Reader
==========


.. image:: https://img.shields.io/pypi/v/csv_reader.svg
        :target: https://pypi.python.org/pypi/csv_reader

.. image:: https://img.shields.io/travis/victormefer/csv_reader.svg
        :target: https://travis-ci.org/victormefer/csv_reader

.. image:: https://readthedocs.org/projects/csv-reader/badge/?version=latest
        :target: https://csv-reader.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Descricao


* Free software: MIT license
* Documentation: https://csv-reader.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
