=======
History
=======

0.1.0 (2018-09-17)
------------------

* First release on PyPI.


0.1.1 (2018-09-19)
------------------

* GCPReader for Sentinel2 csv file
* GCPReader for Landsat csv file