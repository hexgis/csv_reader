#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `csv_reader` package."""


import unittest
import subprocess

from click.testing import CliRunner
from homura import download
from datetime import datetime, timedelta

from csv_reader.gcp_reader import GCPReader
from csv_reader import cli

STORAGE_GCP_URL = 'https://storage.googleapis.com/'
SENTINEL_CSV_URL = STORAGE_GCP_URL + \
    'gcp-public-data-sentinel-2/index.csv.gz'
LANDSAT_CSV_URL = STORAGE_GCP_URL + \
    'gcp-public-data-landsat/index.csv.gz'


def download_input_data(url, output_path):
    try:
        download(url=url, path=output_path)
        return output_path
    except Exception as exc:
        print(exc)
        return False


class TestGCPReader(unittest.TestCase):
    """Tests for `csv_reader` package."""

    def setUp(self):
        """Set up test fixtures."""
        self.start_date = '2016-01-01'
        self.end_date = '2017-01-02'
        self.sentinel2_output_file = download_input_data(
            SENTINEL_CSV_URL, 'tests/sentinel.csv.gz')
        self.landsat_output_file = download_input_data(
            LANDSAT_CSV_URL, 'tests/landsat.csv.gz')

        if not self.sentinel2_output_file:
            self.fail()

        if not self.landsat_output_file:
            self.fail()

    def tearDown(self):
        """Tear down test fixtures, if any."""
        pass

    def test_gcp_reader_dates(self):
        # GCPReader.__check_file_exists
        start_date = datetime.now() - timedelta(days=15)
        start_date = start_date.date().isoformat()
        end_date = datetime.now() + timedelta(days=1)
        end_date = end_date.date().isoformat()

        start, end = GCPReader._GCPReader__get_formatted_dates(
            start_date=None, end_date=None)
        self.assertEqual(start, start_date)
        self.assertEqual(end, end_date)

        start, end = GCPReader._GCPReader__get_formatted_dates(
            start_date=False, end_date=False)
        self.assertEqual(start, start_date)
        self.assertEqual(end, end_date)

        start, end = GCPReader._GCPReader__get_formatted_dates(
            start_date='1970-01-01', end_date=False)
        self.assertEqual(start, '1970-01-01')
        self.assertEqual(end, end_date)

        start, end = GCPReader._GCPReader__get_formatted_dates(
            start_date=None, end_date='1970-01-30')
        self.assertEqual(start, start_date)
        self.assertEqual(end, '1970-01-31')

    def test_gcp_check_file_exists(self):
        file_exists = GCPReader._GCPReader__check_file_exists(
            self.sentinel2_output_file
        )
        self.assertTrue(file_exists)

        self.assertRaises(
            ValueError,
            GCPReader._GCPReader__check_file_exists,
            'pasta/arquivo'
        )

    def test_gcp_check_file_integrity(self):
        file_integrity = GCPReader._GCPReader__check_gzip_integrity(
            self.sentinel2_output_file)
        self.assertTrue(file_integrity)

        subprocess.call('touch tests/data.csv.gz', shell=True)
        self.assertRaises(
            ValueError,
            GCPReader._GCPReader__check_gzip_integrity,
            'tests/data.csv.gz'
        )

    def test_gcp_check_sentinel2_file_headers(self):

        headers = [
            'GRANULE_ID', 'PRODUCT_ID', 'MGRS_TILE', 'BASE_URL',
            'SENSING_TIME', 'CLOUD_COVER', 'GENERATION_TIME',
        ]

        headers_exists = GCPReader._GCPReader__check_gzip_headers(
            self.sentinel2_output_file,
            headers
        )

        self.assertTrue(headers_exists)

        headers = ['GRANULE_ID', 'HEADER_NONEXISTENT']

        self.assertRaises(
            ValueError,
            GCPReader._GCPReader__check_gzip_headers,
            self.sentinel2_output_file,
            headers
        )

    def test_gcp_check_landsat_file_headers(self):

        headers = [
            'SCENE_ID', 'PRODUCT_ID', 'SPACECRAFT_ID',
            'DATE_ACQUIRED', 'COLLECTION_NUMBER', 'WRS_PATH',
            'WRS_ROW', 'CLOUD_COVER', 'BASE_URL'
        ]

        headers_exists = GCPReader._GCPReader__check_gzip_headers(
            self.landsat_output_file, headers
        )

        self.assertTrue(headers_exists)

        headers = ['PRODUCT_ID', 'HEADER_NONEXISTENT']

        self.assertRaises(
            ValueError,
            GCPReader._GCPReader__check_gzip_headers,
            self.sentinel2_output_file,
            headers
        )

    def test_get_sentinel2_data(self):
        """ Test get data from csv for sentinel2 file """
        result = {}

        for data in GCPReader.sentinel2(self.sentinel2_output_file,
                                        self.start_date, self.end_date):
            result = {**result, **data}
            break

        self.assertGreater(len(result), 0)

    def test_get_landsat_data(self):
        """ Test get data from csv for landsat file """
        result = {}

        for data in GCPReader.landsat(self.landsat_output_file,
                                      self.start_date, self.end_date):
            result = {**result, **data}
            break

        self.assertGreater(len(result), 0)

    def test_get_sentinel2_data_filter_area(self):
        """ Test get sentinel 2 data filtered by area tiles """
        result = {}
        area_tiles = ['52LDQ', '52LCP', '52LCR', '52LBQ']

        for data in GCPReader.sentinel2(self.sentinel2_output_file,
                                        self.start_date, self.end_date,
                                        area_tiles=area_tiles):
            result = {**result, **data}
        self.assertEqual(len(result), 124)

    def test_get_landsat_data_filter_area(self):
        """ Test get landsat data filtered by area tiles """
        result = {}
        area_tiles = [('221', '073'), ('221', '074'), ('221', '075')]

        for data in GCPReader.landsat(self.landsat_output_file,
                                      self.start_date, self.end_date,
                                      area_tiles=area_tiles):
            result = {**result, **data}
        self.assertEqual(len(result), 138)

    def test_command_line_interface(self):
        """Test the CLI."""
        runner = CliRunner()
        result = runner.invoke(cli.main)
        assert result.exit_code == 0
        assert 'csv_reader.cli.main' in result.output
        help_result = runner.invoke(cli.main, ['--help'])
        assert help_result.exit_code == 0
        assert '--help  Show this message and exit.' in help_result.output


class TestGCPReaderAcquisition(unittest.TestCase):
    """Tests for `csv_reader` package without downloadeb file"""

    def setUp(self):
        """Set up test fixtures."""
        self.start_date = '2016-01-01'
        self.end_date = '2017-01-02'

    def test_get_sentinel2_data(self):
        """ Test get data from csv for sentinel2 file """
        result = {}

        for data in GCPReader.sentinel2(start_date=self.start_date,
                                        end_date=self.end_date):
            result = {**result, **data}
            break

        self.assertGreater(len(result), 0)

    def test_get_sentinel2_data_filter_area(self):
        """ Test get sentinel 2 data filtered by area tiles """
        result = {}
        area_tiles = ['52LDQ', '52LCP', '52LCR', '52LBQ']

        for data in GCPReader.sentinel2(start_date=self.start_date,
                                        end_date=self.end_date,
                                        area_tiles=area_tiles):
            result = {**result, **data}
        self.assertEqual(len(result), 124)
