.. highlight:: shell

============
Installation
============


Stable release
--------------

To install CSV Reader, run this command in your terminal:

.. code-block:: console

    $ pip install csv_reader

This is the preferred method to install CSV Reader, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for CSV Reader can be downloaded from the `Github repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://github.com/victormefer/csv_reader

Or download the `tarball`_:

.. code-block:: console

    $ curl  -OL https://github.com/victormefer/csv_reader/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Github repo: https://github.com/victormefer/csv_reader
.. _tarball: https://github.com/victormefer/csv_reader/tarball/master
